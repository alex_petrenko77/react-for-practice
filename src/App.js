import React, { useState, useEffect } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import Header from "./component/Header";
import LayoutBlock from "./component/Layout";
import Profile from "./component/Profile";

const App = () => {
  useEffect(() => {
    // Обновляем заголовок документа, используя API браузера
    document.title = `React Practice`;
  }, []);

  return (
    <BrowserRouter>
      <Header />
      <Switch>
        <Route exact path="/" component={LayoutBlock} />
        <Route path="/video/" render={() => <h1>ТИПО ВИДЕО</h1>} />
        <Route path="/profile" render={() => <Profile />} />
      </Switch>
    </BrowserRouter>
  );
};

export default App;

// function useClientRect() {
//   const [rect, setRect] = useState(null);
//   const ref = useCallback(node => {
//     if (node !== null) {
//       setRect(node.getBoundingClientRect());
//     }
//   }, []);
//   return [rect, ref];
// }
