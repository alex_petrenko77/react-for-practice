import React, { useEffect, useState } from "react";
import { useLocation, useHistory } from "react-router-dom";

import style from "../App.module.scss";

const LayoutBlock = props => {
  console.log("props =========>", props);
  const location = useLocation();
  const history = useHistory();
  console.log("location =========>", location);
  console.log("location =========>", history);

  const [state, setState] = useState({
    left: 0,
    top: 0,
    width: 100,
    height: 100
  });

  const [a, setA] = useState(1);
  if (a === 5) {
    // Simulate a JS error
    throw new Error("I crashed!");
  }
  useEffect(() => {
    function handleWindowMouseMove(e) {
      // Использование "...state" гарантирует, что мы не потеряем width и height
      setState(state => ({ ...state, left: e.x, top: e.y }));
    }
    // Примечание: эта реализация немного упрощена
    window.addEventListener("mousemove", handleWindowMouseMove);

    return () => window.removeEventListener("mousemove", handleWindowMouseMove);
  }, []);

  return (
    <div className={style.container}>
      <p>Вы нажали раз</p>
      <button type="button" onClick={() => setA(5)}>
        Нажми на меня и будет ошибка
      </button>
      <div className={style.animation} style={{ color: "purple" }}>
        <button type="button" onClick={() => history.push("/video")}>
          Video link
        </button>
        <h1>{state.left}</h1>
        <h1>{state.top}</h1>
      </div>
    </div>
  );
};

export default LayoutBlock;
