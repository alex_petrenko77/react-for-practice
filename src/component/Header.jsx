import React from "react";
import { AppBar, Button, IconButton, Toolbar } from "@material-ui/core";
import MenuIcon from "@material-ui/core/SvgIcon/SvgIcon";
import { Link } from "react-router-dom";

import styles from "../App.module.scss";

const Links = [
  { to: "/", name: "Main" },
  { to: "/video", name: "Video" },
  { to: "profile", name: "Profile" }
];

const Header = () => (
  <AppBar position="static">
    <Toolbar>
      <IconButton edge="start" color="inherit" aria-label="menu">
        <MenuIcon />
      </IconButton>
      {Links.map(({ to, name }) => (
        <Button key={name} color="primary">
          <Link className={styles.link} to={to}>
            {name}
          </Link>
        </Button>
      ))}
      <Button color="inherit">Login</Button>
    </Toolbar>
  </AppBar>
);

export default Header;
