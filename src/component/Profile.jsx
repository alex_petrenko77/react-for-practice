import React, {
  forwardRef,
  useDebugValue,
  useEffect,
  useLayoutEffect,
  useRef,
  useImperativeHandle,
  useState
} from "react";

import Button from "@material-ui/core/Button";
import styles from "../App.module.scss";

const FancyInput = forwardRef((props, ref) => {
  const inputRef = useRef();
  useImperativeHandle(ref, () => ({
    focus: () => {
      console.log("changed focus behavior=========>");
    }
  }));
  return <input ref={inputRef} type="text" {...props} />;
});

const Profile = () => {
  const [testState, setTestState] = useState(222);

  useEffect(() => {
    console.log(" =========>", "UseEffect");

    return () => console.log(" =========> unmount");
  }, [testState]);

  useLayoutEffect(() => console.log("=========> layout"));

  const ref = useRef(null);

  return (
    <div className={styles.container}>
      <h1>Test page for hooks</h1>
      <h2>{testState}</h2>
      <Button onClick={() => setTestState(10)}>Set page</Button>
      <Button onClick={() => console.log("ref =========>", ref.current.height)}>
        Set page async
      </Button>
      <FancyInput ref={ref} />
    </div>
  );
};

export default Profile;
